// viewport size
function viewport(){var a=window,b="inner";return"innerWidth"in window||(b="client",a=document.documentElement||document.body),{width:a[b+"Width"],height:a[b+"Height"]}}
var winW=viewport().width;
// viewport size

// vars
var seoFix = $(".seo__right-wrap").html();
// vars

/*----------begin doc ready----------*/
$(document).ready(function(){

// ios fix
/Android|webOS|iPhone|iPad|iPod|BlackBerry/i.test(navigator.userAgent)&&$("body").addClass("ios");
// ios fix

// placeholder
$("input, textarea").each(function(){var a=$(this).attr("placeholder");$(this).focus(function(){$(this).attr("placeholder","")}),$(this).focusout(function(){$(this).attr("placeholder",a)})});
// placeholder

// top slider
var slider1 = $(".slider").length;
if(slider1>0){
	$(".slider-1__item, .slider-2__item").each(function(){
        var curInd = $(this).index();
		$(this).attr("data-item",curInd);
    });
	$('.js-slider-1').slick({
		dots: false,
		infinite: true,
		autoplay: true,
		autoplaySpeed: 8000,
		slidesToShow: 1,
		slidesToScroll: 1,
		touchThreshold: 200,
		speed: 700,
		prevArrow: $('.slider-1__prev'),
		nextArrow: $('.slider-1__next'),
		responsive: [
		{breakpoint: 768,settings: {dots: true}}
	  ]
	});
	$('.js-slider-2').slick({
		slidesToShow: 6,
		slidesToScroll: 1,
		dots: false,
		speed: 700,
	  	responsive: [
		{breakpoint: 1101,settings: {slidesToShow: 5}},
		{breakpoint: 1025,settings: {slidesToShow: 4}},
		{breakpoint: 768,settings: {slidesToShow: 3}}
	  ]
	});
	$('.js-slider-1').on('beforeChange', function(event, slick, currentSlide, nextSlide){
		$(".slider-2__fix").addClass("active");
		$(".slider-2__item").removeClass("active");
		$(".slider-2__item[data-item="+nextSlide+"]").addClass("active");
		var swipeDir = slick.swipeDirection();
		if(swipeDir=="left"){
			$(".js-slider-2 .slick-next").click();
		}
		if(swipeDir=="right"){
			$(".js-slider-2 .slick-prev").click();
		}
	});
	$('.js-slider-1').on('afterChange', function(event, slick, currentSlide, nextSlide){
		$(".slider-2__fix").removeClass("active");
		if($(".slider-2__item.active").hasClass("slick-active")){return;}
		else{$(".js-slider-2").slick("slickGoTo",currentSlide);}
	});
	$(".js-slider-link").live("click",function(){
		var curData = $(this).closest(".js-slider-item").data("item");
		$(".js-slider-1").slick("slickGoTo",curData);
	})
	$(".slider-1__prev").live("click",function(){
		$(".js-slider-2 .slick-prev").click();
	})
	$(".slider-1__next").live("click",function(){
		$(".js-slider-2 .slick-next").click();
	})
}
// top slider

// questions
var sliderQ = $('.js-q').length;
if(sliderQ>0){
	$('.js-q').on('afterChange init', function(event, slick, currentSlide, nextSlide){
		var i = (currentSlide ? currentSlide : 0) + 1;
		$('.questions__dots').text(i + ' из ' + slick.slideCount);
	});
	$('.js-q').slick({
	  dots: true,
	  infinite: false,
	  autoplay: false,
	  slidesToShow: 1,
	  slidesToScroll: 1,
	  touchThreshold: 200,
	  speed: 500,
	  prevArrow: $('.questions__prev'),
	  nextArrow: $('.questions__next'),
	  appendDots: $(".questions__mob-dots"),
	  responsive: [{breakpoint: 768,settings: {prevArrow: $('.questions__p'),nextArrow: $('.questions__n')}}]
	});
}
// questions

// side list
var sliderSide = $('.js-side-list').length;
if(sliderSide>0){
	$('.js-side-list').slick({
	  dots: false,
	  infinite: true,
	  autoplay: false,
	  slidesToShow: 1,
	  slidesToScroll: 1,
	  touchThreshold: 200,
	  speed: 500,
	  adaptiveHeight: true
	});
}
// side list

// rating
var rating = $(".js-rating").length;
if(rating>0){
	$('.js-rating').barrating({
		showSelectedRating: false,
		readonly: true
	});
}
// rating

// countdown
var countLen1 = $(".js-timer-1").length;
if(countLen1>0){
	$('.js-timer-1').syotimer({
		year:2017,
		month:8,
		day:05,
		hour:23,
		minute:59,
		dayVisible:true,
		doubleNumbers:true,
		lang:'rus'
	});
}

var countLen2 = $(".js-timer-2").length;
if(countLen2>0){
	$('.js-timer-2').syotimer({
		year:2017,
		month:8,
		day:02,
		hour:6,
		minute:58,
		dayVisible:true,
		doubleNumbers:true,
		lang:'rus'
	});
}
// countdown

// masked input
var maskLen = $(".js-mask").length;
if(maskLen>0){
	$(".js-mask").mask("+7 (999) 999 - 99 - 99");
}
var maskInput = $(".js-mask-input").length;
if(maskInput>0){
	$(".js-mask-input").mask("(9 9 9) 9 9 9 - 9 9 -9 9");
}
// masked input

// tabs
$(".js-tabs-link").live("click",function(e){
	e.preventDefault();
	if($(this).closest(".js-tabs-item").hasClass("active")){return;}
	else{
		$(this).closest(".js-tabs-list").find(".js-tabs-item").removeClass("active");
		$(this).closest(".js-tabs-item").addClass("active");
		var curHref = $(this).attr("href");
		if($(this).hasClass("js-tabs-level-2")){
			$(this).closest(".js-tabs").find(".js-tabs-content").removeClass("active");
		}
		else{
			$(this).closest(".js-tabs").find(".js-tabs-content").not(".js-tabs-level-2").removeClass("active");
		}
		$(curHref).addClass("active");
		if(curHref=="#tab-3"){$(".catalog-2").removeClass("styled");}
		if(curHref=="#tab-4"){$(".catalog-2").addClass("styled");}
		// ellip fix
		var ellipLen13 = $('.js-ellip-13').length;
		if(ellipLen13>0){
			$('.js-ellip-13').ellipsis({lines: 13,ellipClass: 'ellip',responsive: false});
		}
		var ellipLen3 = $('.js-ellip-3').length;
		if(ellipLen3>0){
			$('.js-ellip-3').ellipsis({lines: 3,ellipClass: 'ellip',responsive: false});
		}
		// slider fix
		var respLen = $(".js-resp-slider").length;
		if(respLen>0){
			$(".js-resp-slider").slick('setPosition');			
		}		
	}
})

$(".js-to-tab").live("click",function(){
	var tabData = $(this).data("tab");
	$("."+tabData).click();
})

$(".js-tabs-left").live("click",function(){
	var tabsLen = $(this).closest(".js-tabs").find(".js-tabs-item").length-1;
	var tabsCur = $(this).closest(".js-tabs").find(".js-tabs-item.active").index();
	if(tabsCur==0){$(this).closest(".js-tabs").find(".js-tabs-item:last-child .js-tabs-link").click();}
	else{$(this).closest(".js-tabs").find(".js-tabs-item.active").prev(".js-tabs-item").find(".js-tabs-link").click();}
})
$(".js-tabs-right").live("click",function(){
	var tabsLen = $(this).closest(".js-tabs").find(".js-tabs-item").length-1;
	var tabsCur = $(this).closest(".js-tabs").find(".js-tabs-item.active").index();
	if(tabsLen==tabsCur){$(this).closest(".js-tabs").find(".js-tabs-item:first-child .js-tabs-link").click();}
	else{$(this).closest(".js-tabs").find(".js-tabs-item.active").next(".js-tabs-item").find(".js-tabs-link").click();}
})
// tabs

// tabs tablet
/*
$('.js-tabs-slider').on('beforeChange', function(event, slick, currentSlide, nextSlide){
	$(".calc-nav-4__fix").addClass("active");
	$(".js-slide-item").removeClass("active");
	$(".js-slide-item[data-item="+nextSlide+"]").addClass("active");
});
$('.js-tabs-slider').on('afterChange', function(event, slick, currentSlide, nextSlide){
	$(".calc-nav-4__fix").removeClass("active");
});
$(".js-slide-link").live("click",function(){
	$(".js-slide-item").removeClass("active");
	$(this).closest(".js-slide-item").addClass("active");
	var curData = $(this).closest(".js-slide-item").data("item");
	$(".js-tabs-slider").slick("slickGoTo",curData);
})
*/
// tabs tablet

// ellip
var ellipLen2 = $('.js-ellip-2').length;
if(ellipLen2>0){
	setTimeout(function(){$('.js-ellip-2').ellipsis({lines: 2,ellipClass: 'ellip',responsive: false});}, 300);
}
var ellipLen3 = $('.js-ellip-3').length;
if(ellipLen3>0){
	setTimeout(function(){$('.js-ellip-3').ellipsis({lines: 3,ellipClass: 'ellip',responsive: false});}, 300);
}
var ellipLen4 = $('.js-ellip-4').length;
if(ellipLen4>0){
	setTimeout(function(){$('.js-ellip-4').ellipsis({lines: 4,ellipClass: 'ellip',responsive: false});}, 300);
}
var ellipLen5 = $('.js-ellip-5').length;
if(ellipLen5>0){
	setTimeout(function(){$('.js-ellip-5').ellipsis({lines: 5,ellipClass: 'ellip',responsive: false});}, 300);
}
var ellipLen10 = $('.js-ellip-10').length;
if(ellipLen10>0){
	setTimeout(function(){$('.js-ellip-10').ellipsis({lines: 10,ellipClass: 'ellip',responsive: false});}, 300);
}
var ellipLen13 = $('.js-ellip-13').length;
if(ellipLen13>0){
	setTimeout(function(){$('.js-ellip-13').ellipsis({lines: 13,ellipClass: 'ellip',responsive: false});}, 300);
}
// ellip

// item fix
$(".item-1__main, .item-2__main").live("mouseover",function(){
	$(this).addClass("fixed-1");
	$(".item-1__main, .item-2__main").not(".fixed-1").addClass("fixed-2");
}).live("mouseleave",function(){
	$(".item-1__main, .item-2__main").removeClass("fixed-1").removeClass("fixed-2");
})
// item fix

// drag
$(".js-drag-link").live("click",function(){
	$(this).closest(".js-drag").find(".js-drag-item").removeClass("active");
	$(this).closest(".js-drag-item").addClass("active");
	var position1 = $(this).closest(".js-drag-item").position();
	var position2 = position1.left;
	var curW = $(this).closest(".js-drag-item").width();
	var curHref = $(this).attr("href");
	$(this).closest(".js-drag").find(".js-drag-fix").css("left",position2+"px").css("width",curW+"px");
})
// drag

// focus fix
$(".focus-fix").click(function(){
	$(this).next("input").focus();
	$(this).prev("input").focus();
})
// focus fix

// windows
$(".js-focus").live("focus",function(){
	$(this).closest(".js-windows").find(".js-window").removeClass("active");
	var curData = $(this).data("item");
	if(curData==1){$(this).closest(".js-windows").find(".js-window[data-item='1']").addClass("active");}
	if(curData==2){$(this).closest(".js-windows").find(".js-window[data-item='2']").addClass("active");}
}).live("blur",function(){
	$(this).closest(".js-windows").find(".js-window").removeClass("active");
	$(this).closest(".js-windows").find(".js-window[data-item='0']").addClass("active");
})
// windows

// more seo
$(".js-seo").live("click",function(){
	$(".seo__right-wrap").html(seoFix);
	$(".seo").addClass("active");
})
// more seo

// js slide
$(".js-slide-button").live("click",function(e){
	$(this).toggleClass("active");
	$(this).closest(".js-slide-wrap").toggleClass("active");
	$(this).closest(".js-slide-wrap").find(".js-slide-hide").stop().slideToggle(200,function(){
		if($(".js-float-wrap").length > 0) {
			floatbar();
		}
	});
	e.preventDefault();
})

$(".js-slide-wrap.active").each(function(){
    $(this).find(".js-slide-button").addClass("active");
	$(this).find(".js-slide-hide").slideDown(0,function(){
		if($(".js-float-wrap").length > 0) {
			floatbar();
		}		
	});
});
// js slide

// js fade
$(".js-fade-button").live("mouseenter",function(){
	if($(this).hasClass("active")){return}
	else{
		$(".js-fade-hide, .js-fade-fix").stop().fadeOut(0);
		$(".js-fade-wrap, .js-fade-button").removeClass("active");	
		$(this).closest(".js-fade-wrap").find(".js-fade-hide, .js-fade-fix").stop().fadeIn(300);
		$(this).removeClass("fixed");
		$(this).addClass("active");
	}
})
$(".js-fade-out").live("mouseleave",function(){
	$(".js-fade-hide, .js-fade-fix").stop().fadeOut(0);
	$(".js-fade-wrap, .js-fade-button").removeClass("active");
})

$(".js-fade-button").live("click",function(){
	if($(this).hasClass("fixed")){
		$(this).removeClass("fixed");
		$(this).closest(".js-fade-wrap").find(".js-fade-hide, .js-fade-fix").stop().fadeIn(300);			
	}
	else{
		$(this).addClass("fixed");
		$(this).closest(".js-fade-wrap").find(".js-fade-hide, .js-fade-fix").stop().fadeOut(0);		
	}
})

$(".ios .js-fade-button").click(function(e){e.preventDefault();})
// js fade

// art mob fix
$('.js-articles').on('translated.owl.carousel',function(e){
	var artFix1 = $(".js-art-fix-1").height();
	$(".bottom").css("padding-bottom",artFix1+"px");
});
// art mob fix

// mob nav
function toggleMobNav(){
	if($(this).hasClass("active")){
		$(this).removeClass("active");
		$(".main-wrap").stop().animate({left: '0'}, 300, function(){
			$("body").removeClass("body-mob");
			$(".main-wrap").css("width","100%");
			var slideLen = $(".js-slider-1").length;
			if(slideLen>0){
				$('.js-slider-1').slick('slickPlay');
			}
		});
	}
	else{
		var curW = $(".main-wrap").width();
		$(".main-wrap").css("width",curW+"px").stop().animate({left: '260px'}, 300);
		$(this).addClass("active");
		$("body").addClass("body-mob");
		var slideLen = $(".js-slider-1").length;
		if(slideLen>0){
			$('.js-slider-1').slick('slickPause');
		}
	}
}

$(".js-mob").click(toggleMobNav);

var copyMob1 = $(".mob-nav__lev-1 .mob-nav__bottom").html();
$(".mob-nav__bottom").html(copyMob1);

var copyMob2 = $(".mob-nav__copy").html();
$(".mob-nav__paste").html(copyMob2);

$(".js-mob-to-2").live("click",function(e){
	e.preventDefault();
	$(".mob-nav__scroll").addClass("lev-2").removeClass("lev-3");

	var curTitle = $(this).find(".js-mob-title").text();
	$(".js-mob-title-1").text(curTitle);

	var curData = $(this).attr("data-slide");
	curSlide = $("#"+curData).html();
	$(".mob-nav__lev-2 .mob-nav__change").html(curSlide);

	var curH = $(".mob-nav__lev-2").height();
	$(".mob-nav__all").css("height",curH+"px");
})

$(".js-mob-to-3").live("click",function(e){
	e.preventDefault();
	$(".mob-nav__scroll").addClass("lev-3").removeClass("lev-2");

	var curTitle = $(this).find(".js-mob-title").text();
	$(".js-mob-title-2").text(curTitle);

	var curData = $(this).attr("data-slide");
	curSlide = $("#"+curData).html();
	$(".mob-nav__lev-3 .mob-nav__change").html(curSlide);

	var curH = $(".mob-nav__lev-3").height();
	$(".mob-nav__all").css("height",curH+"px");
})

$(".js-mob-back-1").live("click",function(e){
	e.preventDefault();
	$(".mob-nav__scroll").removeClass("lev-2").removeClass("lev-3");
	var curH = $(".mob-nav__lev-1").height();
	$(".mob-nav__all").css("height",curH+"px");

	var curH = $(".mob-nav__lev-1").height();
	$(".mob-nav__all").css("height",curH+"px");
})

$(".js-mob-back-2").live("click",function(e){
	e.preventDefault();
	$(".mob-nav__scroll").addClass("lev-2").removeClass("lev-3");
	var curH = $(".mob-nav__lev-2").height();
	$(".mob-nav__all").css("height",curH+"px");

	var curH = $(".mob-nav__lev-2").height();
	$(".mob-nav__all").css("height",curH+"px");
})
// mob nav

// scroll top
$(".js-scroll-top").click(function(){ $('html, body').animate({scrollTop: "0px"}); });
// scroll top

// show more mob
$(".js-show-mob.fixed").live("click",function(e){
	e.preventDefault();
	if($(this).hasClass("active")){
		$(this).removeClass("active");
		$(".calc-orders").stop().slideUp(200);
	}
	else{
		$(this).addClass("active");
		$(".calc-orders").stop().slideDown(200);
	}
})
// show more mob

// popups
$(".js-icon-close").live("click",function(){
	$(".fancybox-close-small").click();
})

$(".js-popup-mob").live("click",function(){
	$(".js-mob").removeClass("active");
	$(".main-wrap").stop().animate({left: '0'}, 300, function(){
		$("body").removeClass("body-mob");
		$(".main-wrap").css("width","100%");
	});
	var slideLen = $(".js-slider-1").length;
	if(slideLen>0){
		$('.js-slider-1').slick('slickPlay');
	}	
})
// popups

// remove item
$(".js-remove-button").live("click",function(){
	$(this).closest(".js-remove-item").fadeOut(300,function(){
		$(this).remove();
	})
})
// remove item

// plus minus
$(".plus-minus__next").live("click",function(){var t=$(this).prev().val();$(this).prev().val(+t+1);return false;}),$(".plus-minus__prev").live("click",function(){var t=$(this).next().val();t>1&&$(this).next().val(+t-1);return false;});
$(".plus-minus__input").keypress(function(e){var i,n;if(!e)var e=window.event;return e.keyCode?i=e.keyCode:e.which&&(i=e.which),null==i||0==i||8==i||13==i||9==i||46==i||37==i||39==i?!0:(n=String.fromCharCode(i),/\d/.test(n)?void 0:!1)});			
// plus minus

// focus fix
$(".js-focus-fix").click(function(e){
	e.preventDefault();
	$(this).closest(".js-focus").find("textarea, input").focus();
})
$(".js-focus textarea, .js-focus input").focus(function(){
	$(this).closest(".js-focus").find(".js-focus-fix").fadeOut(0); 	
})
$(".js-focus textarea, .js-focus input").blur(function(){
	if($(this).val().trim().length <1){
		$(this).closest(".js-focus").find(".js-focus-fix").fadeIn(0);	
	}		
})
// focus fix

// js more
$(".js-more-button").live("click",function(){
	$(this).closest(".js-more").find(".js-more-hide, .js-more-text-1, .js-more-text-2").fadeToggle(0);
	if($(this).parent("*").hasClass("basket-item")){
		$(this).toggleClass("active");
	}
	var questLen1 = $(".js-q").length;
	if(questLen1>0){
		$(".js-q").slick('setPosition');			
	}	
	var questLen2 = $(".js-q-in").length;
	if(questLen2>0){
		$(".js-q-in").slick('setPosition');			
	}		
	var respLen = $(".js-q").length;
	if(respLen>0){
		$(".js-responses").trigger('refresh.owl.carousel');		
	}
})
// js more

// responses gallery
var galleryLen = $(".js-resp-gal").length;

if(galleryLen>0){
	$(".resp-gal-1__item, .resp-gal-2__item").each(function(){
        var curInd = $(this).index();
		$(this).attr("data-item",curInd);
    });	
	$('.js-resp-slider').slick({
	  dots: false,
	  infinite: false,
	  autoplay: false,
	  speed: 500,
	  slidesToShow: 1,
	  slidesToScroll: 1,
	  touchThreshold: 200
	});		
	$('.js-resp-slider').on('afterChange', function(event, slick, currentSlide, nextSlide){
		$(".resp-gal-2__fix").fadeOut(0);
	});	
	$(".js-resp-link").live("click",function(){
		$(".resp-gal-2__fix").fadeIn(0);		
		var curData = $(this).closest(".resp-gal-2__item").data("item");
		$(this).closest(".js-resp-gal").find(".js-resp-slider").slick("slickGoTo",curData);	
		$(this).closest(".js-resp-gal").find(".resp-gal-2__item").removeClass("active");
		$(this).closest(".resp-gal-2__item").addClass("active");	
	})
	
}
// responses gallery

// top slider
var sliderInner = $(".js-inner-slider").length;
if(sliderInner>0){
	$('.js-inner-slider').slick({
		dots: true,
		infinite: true,
		autoplay: true,
		autoplaySpeed: 8000,
		slidesToShow: 1,
		slidesToScroll: 1,
		touchThreshold: 200,
		speed: 700,
		prevArrow: $('.inner-slider__prev'),
		nextArrow: $('.inner-slider__next'),
	});
}
// top slider

// 3d images
var len3d = $(".js-3d-item").length;
if(len3d>0){
	$(".js-3d-item").each(function(){
		var rotator;
		var imagepath = $(this).attr("data-path");  //html путь к картинкам
		var imagesCount = $(this).attr("data-images-count"); //html кол во картинок в папке
		rotator = $(this).ThreeSixty({
			totalFrames: imagesCount, // Total no. of image you have for 360 slider
			endFrame: imagesCount, // end frame for the auto spin animation
			currentFrame: 1, // This the start frame for auto spin
			imgList: '.js-images', // selector for image list
			progress: '.js-loader', // selector to show the loading progress
			imagePath:imagepath, // path of the image assets
			ext: '.png', // extention for the assets
			drag:false,
			navigation: false,
			responsive: true,
		});
	})
	
	
	//3D Скрипт , по наведению на элемент
	$(".js-3d-wrap").each(function(index){
		var tl2 = new TimelineMax();
		var tl1 = new TimelineMax();    
		$(this).hover(function(){
			tl1.staggerTo($(this).find(".js-3d-item li:not(:first-child)"), 0, { opacity:1, ease: Power1.easeInOut}, 0.050, 0);
			tl2.staggerTo($(this).find(".js-3d-item li:not(:last-child) img"), 0, { opacity:0, ease: Power1.easeInOut}, 0.050, 0);
			tl1.play();
			tl2.play();
		},function(){
			tl1.reverse();
			tl2.reverse();
		})
	})
}
// 3d images

// head nav
$(".js-nav-link").mouseover(function(){
	var curNumber = $(this).attr("data-item");	
	$(".js-nav-item").removeClass("active");	
	$(".js-nav-item[data-item="+curNumber+"]").addClass("active");	
	$(".js-nav-parent").removeClass("current");
	$(this).closest(".js-nav-parent").addClass("current");
})
// head nav

// questions inner
var qinLen = $('.js-q-in').length;
if(qinLen>0){
	$('.js-q-in').on('afterChange init', function(event, slick, currentSlide, nextSlide){
		var i = (currentSlide ? currentSlide : 0) + 1;
		$('.q-in__dots').text(i + ' из ' + slick.slideCount);
	});
	$('.js-q-in').slick({
	  dots: true,
	  infinite: false,
	  autoplay: false,
	  slidesToShow: 1,
	  slidesToScroll: 1,
	  touchThreshold: 200,
	  speed: 500,
	  prevArrow: $('.q-in__prev'),
	  nextArrow: $('.q-in__next'),
	});
}
// questions inner

// scroll colors
var colLen = $('.js-scroll-col').length;
if(colLen>0){
	$(".js-scroll-col").mCustomScrollbar({
		horizontalScroll:false,
		mouseWheel:{ enable: true },
		scrollButtons:{enable:false},
		advanced:{updateOnContentResize:true},
		advanced:{updateOnBrowserResize:true},
		scrollInertia: 0,
		mouseWheelPixels: 30
	});
}
// scroll colors

// autocomplete
var autoLen = $('.js-autocomplete').length;
if(autoLen>0){
    var locationArray = ['Московская область', 'Москва', 'Ростов'];
    var location = $.map(locationArray, function (location) { return { value: location }; });

    // Initialize autocomplete with local lookup:
    $('.js-autocomplete').devbridgeAutocomplete({
        lookup: location,
    });	
}
// autocomplete

/* datepicker */
if($(".js-datepicker").length>0){
	
	var dateToday = new Date();
	
	$.datepicker.regional['ru'] = {
		closeText: 'Закрыть',
		inline: true,
		prevText: '',
		nextText: '',
		navigationAsDateFormat: true,
		currentText: 'Сегодня',
		monthNames: ['Январь','Февраль','Март','Апрель','Май','Июнь',
		'Июль','Август','Сентябрь','Октябрь','Ноябрь','Декабрь'],
		monthNamesShort: ['января','февраля','марта','апреля','мая','июня',
		'июль','августа','сентября','октября','ноября','декабря'],
		dayNames: ['воскресенье','понедельник','вторник','среда','четверг','пятница','суббота'],
		dayNamesShort: ['вск','пнд','втр','срд','чтв','птн','сбт'],
		dayNamesMin: ['Вс','Пн','Вт','Ср','Чт','Пт','Сб'],
		weekHeader: 'Нед',
		dateFormat:'d M (DD)',
		firstDay: 1,
		minDate: dateToday,
		isRTL: false,
		showMonthAfterYear: false,
		showOtherMonths: true,
		yearSuffix: ''};
	$.datepicker.setDefaults($.datepicker.regional['ru']);
	 
	$(".js-datepicker").datepicker().datepicker("setDate", new Date());
}
/* datepicker */

// fancybox gallery
$("a[rel=img-box-1]").fancybox({
        'transitionIn' : 'none',
        'transitionOut' : 'none',
        'titlePosition' : 'over'
        // 'nextClick' : 'true'
        // 'titleFormat' : function(title, currentArray, currentIndex, currentOpts) {
        //     return '<span id="fancybox-title-over">Image ' + (currentIndex + 1) + ' / ' + currentArray.length + (title.length ? '   ' + title : '') + '</span>';
        // }
    });

$('.services-color-list__item').click(function () {
	$('.services-color-list__item').removeClass('selected-item');
	$(this).addClass('selected-item');
});

})
/*----------doc ready eof----------*/

/* fix scroll2 */
	if($('#services-sidebar').length) {
	    $(function () {
	        var a = document.querySelector('#services-sidebar'),
	            b = null,
	            P = 0;
	        window.addEventListener('scroll', Ascroll, false);
	        document.body.addEventListener('scroll', Ascroll, false);

	        function Ascroll() {
	        	if(viewport().width >= 768) {
	        		if (b == null) {
	        		    var Sa = getComputedStyle(a, ''),
	        		        s = '';
	        		    for (var i = 0; i < Sa.length; i++) {
	        		        if (Sa[i].indexOf('overflow') == 0 || Sa[i].indexOf('padding') == 0 || Sa[i].indexOf('border') == 0 || Sa[i].indexOf('outline') == 0 || Sa[i].indexOf('box-shadow') == 0 || Sa[i].indexOf('background') == 0) {
	        		            s += Sa[i] + ': ' + Sa.getPropertyValue(Sa[i]) + '; '
	        		        }
	        		    }
	        		    b = document.createElement('div');
	        		    b.style.cssText = s + ' box-sizing: border-box; width: ' + a.offsetWidth + 'px;';
	        		    a.insertBefore(b, a.firstChild);
	        		    var l = a.childNodes.length;
	        		    for (var i = 1; i < l; i++) {
	        		        b.appendChild(a.childNodes[1]);
	        		    }
	        		    a.style.height = b.getBoundingClientRect().height + 'px';
	        		    a.style.padding = '0';
	        		    a.style.border = '0';
	        		}
	        		var Ra = a.getBoundingClientRect(),
	        		    R = Math.round(Ra.top + b.getBoundingClientRect().height - document.querySelector('#services-tabs').getBoundingClientRect().bottom);
	        		    var topOffset = (viewport().width >= 1025) ? 59 : 0;
	        		if ((Ra.top - P) <= topOffset) {
	        		    if ((Ra.top - P) <= R) {
	        		        b.className = 'stop';
	        		        b.style.top = -R - 2 + 'px';
	        		    } else {
	        		        b.className = 'sticky';
	        		        b.style.top = P + 'px';
	        		    }
	        		} else {
	        		    b.className = '';
	        		    b.style.top = '';
	        		}
	        		window.addEventListener('resize', function () {
	        		    a.children[0].style.width = getComputedStyle(a, '').width
	        		}, false);
	        	}
	        }
	    });    
	};

$(window).load(function(){
	// body fix
	$('body').removeClass('loaded');
	// body fix

	// responses
	var slider2 = $(".js-responses").length;
	if(slider2>0){
		$('.js-responses').addClass("owl-carousel").owlCarousel({
			loop:true,
			items:3,
			autoplay:false,
			smartSpeed:500,
			margin:0,
			dotsContainer:".responses__dots",
			nav: true,
			navContainer:".responses__nav",
			navText:"",
			autoHeight:true,
			responsive: {0:{items: 1},768: {items: 2},1025: {items: 3}}
		})
	}
	// responses

	// advantages
	var slider5 = $(".js-adv-1").length;
	if(slider5>0){
		$('.js-adv-1').addClass("owl-carousel").owlCarousel({
			loop:true,
			items:4,
			autoplay:false,
			smartSpeed:500,
			margin:0,
			nav: true,
			navContainer:".advantages-1__arrows",
			navText:"",
			autoHeight:true,
			responsive: {0:{items: 1},768: {items: 3},1025: {items: 4}}
		})
	}

	var slider6 = $(".js-adv-2").length;
	if(slider6>0){
		$('.js-adv-2').addClass("owl-carousel").owlCarousel({
			loop:true,
			items:3,
			autoplay:false,
			smartSpeed:500,
			margin:0,
			nav: true,
			navContainer:".advantages-2__arrows",
			navText:"",
			autoHeight:true,
			responsive: {0:{items: 1},768: {items: 3}}
		})
	}
	// advantages
	
	// responses
	var slider7 = $(".js-news").length;
	if(slider7>0){
		var itemsLen = $(".news-3__item").length;
		if(itemsLen<=4){
			$(".news-3__arrows").addClass("news-3__arrows_hide");
		}
		$('.js-news').addClass("owl-carousel").owlCarousel({
			loop:true,
			items:4,
			autoplay:false,
			smartSpeed:500,
			margin:0,
			nav: true,
			navContainer:".news-3__arrows",
			navText:"",
			autoHeight:true,
			responsive: {0:{items: 1},501:{items: 2},768: {items: 3},1101: {items: 4}}
		})
	}
	// responses	
})

/*----------begin bind win load----------*/
var handler1 = function(){
	// formstyler
	var formLen1 = $("input[type=radio]").length;
	var formLen2 = $("input[type=checkbox]").length;
	var formLen3 = $("select").length;
	if(formLen1>0||formLen2>0){
		$('input[type=checkbox], select').styler({});
		$('.radio input[type=radio]').styler({wrapper:'.radio'});
		$(".jq-selectbox__dropdown").each(function(){
			var liLen = $(this).find("li").length;
			if(liLen>8){
				$(this).find(".jq-scroll").mCustomScrollbar({
					horizontalScroll:false,
					scrollButtons:{enable:false},
					advanced:{updateOnContentResize:true},
					advanced:{updateOnBrowserResize:true},
					scrollInertia: 0,
					mouseWheelPixels: 30
				});	
			}
		});		
	}
	// formstyler
}
$(window).bind('load', handler1);
/*----------bind load eof----------*/

/*----------begin bind load & resize & orientation eof----------*/
var handler2 = function(){
// scroll fix
var winW=viewport().width;
if(winW>1400){
	$("body").addClass("scroll-1");
}
if(winW<=1400){
	$("body").removeClass("scroll-1");
}
// scroll fix

// footer fix
setTimeout(function(){
var footLen = $(".footer").length;
if(footLen<1){
	$(".main-wrap").css("padding-bottom","0");
}
else{
	var footH = $(".footer").height();
	$(".main-wrap").css("padding-bottom",footH+"px");
}
}, 1);
// footer fix

// new fix
$(".js-new").each(function(){
    var curW = $(this).find(".js-new-text").width()+12;
	$(this).find(".js-new-button").css("left",curW+"px");
});
setTimeout(function(){
	$(".js-new").each(function(){
		var curW = $(this).find(".js-new-text").width()+12;
		$(this).find(".js-new-button").css("left",curW+"px");
	});
}, 500);
// new fix

// drag fix
$(".js-drag").each(function(e){
	var position1 = $(this).find(".js-drag-item.active").position();
	var position2 = position1.left;
	var curW = $(this).find(".js-drag-item.active").width();
	$(this).find(".js-drag-fix").css("left",position2+"px").css("width",curW+"px");
})

$(".switch__fix").css("width","50%");
$(".basket-nav__fix").css("width","50%");
// drag fix

// columns
$('.js-col').css("min-height","0");
$(".js-cols").each(function(){
	var maxHeight1 = -1;
	$(this).find('.js-col').each(function() {maxHeight1 = maxHeight1 > $(this).height() ? maxHeight1 : $(this).height();});
	$(this).find('.js-col').each(function() {$(this).css("min-height",maxHeight1+"px");});
})
// columns

// seo fix
var winW=viewport().width;
if(winW>1024){$(".seo__right-wrap").html(seoFix);}
if(winW<=1024&&winW>767){$(".seo__right").addClass("js-ellip-10");}
if(winW<=767){$(".seo__right").addClass("js-ellip-5");	}

// seo fix

// ellip
var ellipLen2 = $('.js-ellip-2').length;
if(ellipLen2>0){
	$('.js-ellip-2').ellipsis({lines: 2,ellipClass: 'ellip',responsive: false});
}
var ellipLen3 = $('.js-ellip-3').length;
if(ellipLen3>0){
	$('.js-ellip-3').ellipsis({lines: 3,ellipClass: 'ellip',responsive: false});
}
var ellipLen4 = $('.js-ellip-4').length;
if(ellipLen4>0){
	$('.js-ellip-4').ellipsis({lines: 4,ellipClass: 'ellip',responsive: false});
}
var ellipLen5 = $('.js-ellip-5').length;
if(ellipLen5>0){
	$('.js-ellip-5').ellipsis({lines: 5,ellipClass: 'ellip',responsive: false});
}
var ellipLen10 = $('.js-ellip-10').length;
if(ellipLen10>0){
	$('.js-ellip-10').ellipsis({lines: 10,ellipClass: 'ellip',responsive: false});
}
var ellipLen13 = $('.js-ellip-13').length;
if(ellipLen13>0){
	$('.js-ellip-13').ellipsis({lines: 13,ellipClass: 'ellip',responsive: false});
}
// ellip

// how fix
var howH = $(".how__center-wrap").height();
$(".how__list").css("min-height",howH+"px");
// how fix

// tabs tablet
/*
var winW=viewport().width;
if(winW>1024||winW<=767){
	var sliderTabs = $('.js-tabs-slider .slick-track').length;
	if(sliderTabs){
		$('.js-tabs-slider').slick('unslick');
		$(".js-tabs-slider *").removeAttr("tabindex").removeAttr("role").removeAttr("aria-describedby");
	}
}
if(winW<=1024&&winW>767){
	var sliderTabs = $('.js-tabs-slider').length;
	var sliderTrack = $('.js-tabs-slider .slick-track').length;
	if(sliderTabs>0&&sliderTrack<1){
			$('.js-tabs-slider').slick({
			dots: false,
			infinite: true,
			autoplay: false,
			slidesToShow: 1,
			slidesToScroll: 1,
			touchThreshold: 200,
			speed: 300,
			prevArrow: $('.slider-1__prev'),
			nextArrow: $('.slider-1__next'),
			adaptiveHeight: true
		});
	}
}
*/
// tabs tablet

// scroll
var scrLen = $('.js-scroll').length;
if(scrLen>0){
	var winW=viewport().width;
	if(winW>767){
		$(".js-scroll").mCustomScrollbar({
			horizontalScroll:false,
			mouseWheel:{ enable: true },
			scrollButtons:{enable:false},
			advanced:{updateOnContentResize:true},
			advanced:{updateOnBrowserResize:true},
			scrollInertia: 0,
			mouseWheelPixels: 30
		});
	}
	if(winW<=767){
		$(".js-scroll").mCustomScrollbar("destroy",true);
	}
}
// scroll

// tabs fix
var winW=viewport().width;
if(winW>1024){
	if($(".calc-tabs__item.active").hasClass("calc-tabs__item_hide")){
		$(".calc-tabs__item").removeClass("active");
		$(".calc-nav-3__item").removeClass("active");
		$(".calc-tabs__item:first-child").addClass("active");
		$(".calc-nav-3__item:first-child").addClass("active");
	}
}
// tabs fix

// side mob fix
var winW=viewport().width;
if(winW>767){
	$(".js-show-mob").removeClass("fixed");
}
if(winW<=767){
	$(".js-show-mob").addClass("fixed");
}
// side mob fix

// questions
/*
var winW=viewport().width;
if(winW>767){
	var sliderQ = $('.js-q').length;
	var slickTrack = $('.js-q .slick-track').length;
	if(sliderQ>0&&slickTrack<1){
		var qTrack = $('.js-q .owl-stage').length;
		if(qTrack>0){
			$('.js-q').removeClass("owl-carousel").trigger('destroy.owl.carousel');
		}
		$('.js-q').on('afterChange init', function(event, slick, currentSlide, nextSlide){
			var i = (currentSlide ? currentSlide : 0) + 1;
			$('.questions__dots').text(i + ' из ' + slick.slideCount);
		});
		$('.js-q').slick({
		  dots: true,
		  infinite: true,
		  autoplay: false,
		  slidesToShow: 1,
		  slidesToScroll: 1,
		  touchThreshold: 200,
		  speed: 500,
		  adaptiveHeight: true,
		  prevArrow: $('.questions__prev'),
		  nextArrow: $('.questions__next')
		});
	}
}
if(winW<=767){
	var sliderQ = $('.js-q').length;
	var qTrack = $('.js-q .owl-stage').length;
	if(sliderQ>0&&qTrack<1){
		var slickTrack = $('.js-q .slick-track').length;
		if(slickTrack>0){
			$('.js-q').slick('unslick');
			$(".js-q *").removeAttr("tabindex").removeAttr("role").removeAttr("aria-describedby");
		}
		$('.js-q').addClass("owl-carousel").owlCarousel({
			loop:true,
			items:1,
			autoplay:false,
			smartSpeed:500,
			margin:0,
			nav: true,
			navText:"",
			autoHeight:true
		})
	}
}
*/
// questions

// articles slider
var winW=viewport().width;
if(winW>1024){
	var sliderFoot = $('.js-articles').length;
	if(sliderFoot>0){
		$('.js-articles').removeClass("owl-carousel").trigger('destroy.owl.carousel');
	}
}
if(winW<=1024){
	var sliderFoot = $('.js-articles').length;
	var sliderTrack = $('.js-articles .owl-stage').length;
	if(sliderFoot>0&&sliderTrack<1){
		$('.js-articles').addClass("owl-carousel").owlCarousel({
			loop:true,
			items:3,
			autoplay:false,
			smartSpeed:500,
			margin:0,
			nav: true,
			navContainer:".articles-1__arrows",
			navText:"",
			autoHeight:true,
			responsive: {0:{items: 1},768: {items: 3}}
		})
	}
}
// articles slider

// footer slider
var winW=viewport().width;
if(winW>1024){
	var sliderFoot = $('.js-foot-slider').length;
	if(sliderFoot>0){
		$('.js-foot-slider').removeClass("owl-carousel").trigger('destroy.owl.carousel');
	}
}
if(winW<=1024){
	var sliderFoot = $('.js-foot-slider').length;
	var sliderTrack = $('.js-foot-slider .owl-stage').length;
	if(sliderFoot>0&&sliderTrack<1){
		$('.js-foot-slider').addClass("owl-carousel").owlCarousel({
			loop:true,
			items:3,
			autoplay:false,
			smartSpeed:500,
			margin:0,
			nav: true,
			navContainer:".foot-info__arrows",
			navText:"",
			autoHeight:true,
			responsive: {0:{items: 1},768: {items: 3}}
		})
	}
}
// footer slider

// art mob fix
var artFix1 = $(".js-art-fix-1").height();
$(".bottom").css("padding-bottom",artFix1+"px");

var artFix2 = $(".js-art-fix-2").height()+20;
$(".articles-2").css("padding-top",artFix2+"px");

setTimeout(function(){
var artFix11 = $(".js-art-fix-1").height();
$(".bottom").css("padding-bottom",artFix11+"px");

var artFix22 = $(".js-art-fix-2").height()+20;
$(".articles-2").css("padding-top",artFix22+"px");
}, 500);
// art mob fix

// nav mob fix
var winW=viewport().width;
if(winW>1024){
	$(".js-mob").removeClass("active");
	$(".main-wrap").stop().animate({left: '0'}, 0, function(){
		$("body").removeClass("body-mob");
		$(".main-wrap").css("width","100%");
	});
	var slideLen = $(".js-slider-1").length;
	if(slideLen>0){
		$('.js-slider-1').slick('slickPlay');
	}
}
if(winW<=1024){
	if($(".js-mob").hasClass("active")){
		$(".main-wrap").css("width","100%");
	}
}
// nav mob fix

// about fix
var aboutH = $(".sidebar-4").height()+20;
$(".about__main").css("padding-top",aboutH+"px");
// about fix

// filters fix
var sidebarH = $(".about__left").height();
$(".filters-1").css("min-height",sidebarH+"px");
// filtes fix

// prices table
var pricesLen = (".js-eq").length;
if(pricesLen>0){	
	$(".js-eq-item").css("min-height","auto");
	for (var i=1; i<99; i++){
		var height2 = 0; 	
		$('.js-eq-wrap:nth-child('+i+') .js-eq-item').each(function() {height2 = height2 > $(this).height() ? height2 : $(this).height();});	
		$('.js-eq-wrap:nth-child('+i+') .js-eq-item').each(function() {$(this).css("min-height",height2+"px")});		
	}			
}
// prices table

// prices scroll
var scrLen = $(".pr-table").length;
if(scrLen>0){
	var winW=viewport().width;
	if(winW>1280){
		$(".pr-table-2__main").removeClass("scrolled").removeClass("js-hor-scroll");
		$(".js-hor-scroll").mCustomScrollbar('destroy');
		// fix
		$(".js-scroll").mCustomScrollbar({
			horizontalScroll:false,
			mouseWheel:{ enable: true },
			scrollButtons:{enable:false},
			advanced:{updateOnContentResize:true},
			advanced:{updateOnBrowserResize:true},
			scrollInertia: 0,
			mouseWheelPixels: 30
		});	
		$(".js-scroll-col").mCustomScrollbar({
			horizontalScroll:false,
			mouseWheel:{ enable: true },
			scrollButtons:{enable:false},
			advanced:{updateOnContentResize:true},
			advanced:{updateOnBrowserResize:true},
			scrollInertia: 0,
			mouseWheelPixels: 30
		});		
	}
	if(winW<=1280){
		$(".pr-table-2__main").addClass("scrolled").addClass("js-hor-scroll");
		$(".js-hor-scroll").mCustomScrollbar({
			horizontalScroll:true,
			verticalScroll:true,
			mouseWheel:{ enable: true },
			scrollButtons:{enable:false},
			advanced:{updateOnContentResize:true},
			advanced:{updateOnBrowserResize:true},
			scrollInertia: 0,
			mouseWheelPixels: 30
		});	
	}
}
// prices scroll

// advantages inner slider
var winW=viewport().width;
if(winW>1024){
	var sliderAdv = $('.js-adv-3').length;
	if(sliderAdv>0){
		$('.js-adv-3').removeClass("owl-carousel").trigger('destroy.owl.carousel');
	}
}
if(winW<=1024){
	var sliderAdv = $('.js-adv-3').length;
	var advTrack = $('.js-adv-3 .owl-stage').length;
	if(sliderAdv>0&&advTrack<1){
		$('.js-adv-3').addClass("owl-carousel").owlCarousel({
			loop:true,
			items:3,
			autoplay:false,
			smartSpeed:500,
			margin:0,
			nav: true,
			navContainer:".advantages-4__arrows",
			navText:"",
			autoHeight:true,
			responsive: {0:{items: 1},768: {items: 3}}
		})
	}
}
// advantages inner slider
}
$(window).bind('orientationchange', handler2);
$(window).bind('resize', handler2);
$(window).bind('load', handler2);
/*----------bind load & resize & orientation eof----------*/

/*----------begin bind load & click----------*/
var handler3 = function(){
// formstyler fix
$(".radio__item").each(function(){var a=$(this).find(".jq-radio.checked").length;a>=1?$(this).addClass("active"):$(this).removeClass("active")}),$(".radio__item").each(function(){var a=$(this).find(".jq-radio.disabled").length;a>=1?$(this).addClass("disabled"):$(this).removeClass("disabled")}),$(".checkbox__item").each(function(){var a=$(this).find(".jq-checkbox.checked").length;a>=1?$(this).addClass("active"):$(this).removeClass("active")}),$(".checkbox__item").each(function(){var a=$(this).find(".jq-checkbox.disabled").length;a>=1?$(this).addClass("disabled"):$(this).removeClass("disabled")});
// formstyler fix
}
$(window).bind('click', handler3);
$(window).bind('load', handler3);
/*----------bind load & click eof----------*/

/*----------begin touch----------*/
$(document).on('touchstart', function(){documentClick = true;});
$(document).on('touchmove', function(){documentClick = false;});
$(document).on('click touchend', function(event){
	if(event.type == "click") documentClick = true;
	if(documentClick){
		var target = $(event.target);
		if(target.is('.js-mob')){return}
		if(target.is('.js-mob *')){return}
		if(target.is('.mob-nav')){return}
		if(target.is('.mob-nav *')){return}
		if(target.is('.js-fade-wrap *')){return}	
		if(target.is('.js-slider-2 .slick-prev')){return}	
		if(target.is('.js-slider-2 .slick-next')){return}	
		else{
			$(".js-fade-hide, .js-fade-fix").stop().fadeOut(0);$(".js-fade-button, .js-fade-wrap").removeClass("active").removeClass("fixed");
			$(".js-mob").removeClass("active");
			$(".main-wrap").stop().animate({left: '0'}, 300, function(){
				$("body").removeClass("body-mob");
				$(".main-wrap").css("width","100%");
			});
			var slideLen = $(".js-slider-1").length;
			if(slideLen>0){
				$('.js-slider-1').slick('slickPlay');
			}
		}
	}
});
/*----------touch eof----------*/

/* scroll buttons */
$(window).scroll(function(){
	if($("body").hasClass("scroll-1")){
		if( $(window).scrollTop() >= 285 ) {$(".fixed-but").addClass("active");}
		else{$(".fixed-but").removeClass("active"); }
	}
	else{
		if( $(window).scrollTop() >= 156 ) {$(".fixed-but").addClass("active");}
		else{$(".fixed-but").removeClass("active");}
	}
});

$(window).load(function(){
	if($("body").hasClass("scroll-1")){
		if( $(window).scrollTop() >= 285 ) {$(".fixed-but").addClass("active");}
		else{$(".fixed-but").removeClass("active"); }
	}
	else{
		if( $(window).scrollTop() >= 156 ) {$(".fixed-but").addClass("active");}
		else{$(".fixed-but").removeClass("active"); }
	}
});
/* scroll buttons */

/* scroll header */
$(window).scroll(function(){
	if( $(window).scrollTop() >= 130 ) {$(".fixed-head").stop().fadeIn(0);}
	else{$(".fixed-head").stop().fadeOut(0);}
});

$(window).load(function(){
	if( $(window).scrollTop() >= 130 ) {$(".fixed-head").stop().fadeIn(0);}
	else{$(".fixed-head").stop().fadeOut(0);}
});
/* scroll header */

/* show scroll button */
$(window).scroll(function(){
	if( $(window).scrollTop() >= 1000 ) {$(".fixed-but__scroll").stop().fadeIn(300);$(".fixed-but").addClass("styled");}
	else{$(".fixed-but__scroll").stop().fadeOut(0);$(".fixed-but").removeClass("styled");}
});

$(window).load(function(){
	if( $(window).scrollTop() >= 1000 ) {$(".fixed-but__scroll").stop().fadeIn(300);$(".fixed-but").addClass("styled");}
	else{$(".fixed-but__scroll").stop().fadeOut(0);$(".fixed-but").removeClass("styled");}
});
/* show scroll button */

/* smooth animation
$('.js-anim-item').each(function(){
    var div = $(this).find('.js-anim-bg');
	function animToClose(){
		div.removeClass('unhovered').addClass('hovered animate').on('animationend webkitAnimationEnd', function() {
		   unAnimate($(this));
		});
	}
	function animToOpen(){
		div.removeClass('hovered').addClass('unhovered animate').on('animationend webkitAnimationEnd', function() {
			unAnimate($(this));
		});
	}
	function unAnimate(el){
		el.removeClass('animate');
	}

    $(this).hover(function(){
		if (!div.hasClass('animate')) {
			animToClose();
		}
    },
    function(){
		if (!div.hasClass('animate')) {
			animToOpen();
		} else {
			setTimeout(function(){
				animToOpen();
				unAnimate(div);
			},900);
		}
    });

});
/* smooth animation */

// copyrights
document.oncopy = function () {
	var bodyElement = document.body;
	var selection = getSelection();
	var href = document.location.href;
	var copyright = "<br><br>Источник: <a href='"+ href +"'>" + href + "</a><br>&copy; MFS";
	var text = selection + copyright;
	var divElement = document.createElement('div');
	divElement.style.position = 'absolute';
	divElement.style.left = '-99999px';
	divElement.innerHTML = text;
	bodyElement.appendChild(divElement);
	selection.selectAllChildren(divElement);
	setTimeout(function() {
	bodyElement.removeChild(divElement);
	}, 0);
};
// copyrights

// scroll table
function tablescrolled(){
	var winW=viewport().width;
	if(winW>1024){
		var position1 = $(".js-fixed-wrap").position();
		var positionTop1 = position1.top;	
		var fixedHead = $(".fixed-head").height();
		var position2 = positionTop1-fixedHead+17;
		var headerHeight = $(".header").height();
		var position3 = position2+headerHeight;
		var scrollTop = $(window).scrollTop();
		var position4 = scrollTop-position3;
		var fixedHeight = $(".js-fixed-wrap").height();
		var scrollHeight = $(".js-fixed-tab").height();
		var scrollBottom = positionTop1+fixedHeight;
		if($(window).scrollTop() >= position3){
			$(".js-fixed-tab").css("top",position4+"px").css("z-index","5").addClass("active");
			if($(window).scrollTop() >= scrollBottom){
				$(".js-fixed-tab").css("top",fixedHeight-scrollHeight-37+"px");	
			}
		}
		else{
			$(".js-fixed-tab").css("top","0").css("z-index","auto").removeClass("active");
		}		
	}
	if(winW<=1024){
		var position1 = $(".js-fixed-wrap").position();
		var positionTop1 = position1.top;	
		var position2 = positionTop1+17;
		var headerHeight = $(".header").height();
		var position3 = position2+headerHeight;
		var scrollTop = $(window).scrollTop();
		var position4 = scrollTop-position3;
		var fixedHeight = $(".js-fixed-wrap").height();
		var scrollHeight = $(".js-fixed-tab").height();
		var scrollBottom = positionTop1+fixedHeight-scrollHeight+headerHeight-20;
		if($(window).scrollTop() >= position3){
			$(".js-fixed-tab").css("top",position4+"px").css("z-index","5").addClass("active");
			if($(window).scrollTop() >= scrollBottom){
				$(".js-fixed-tab").css("top",fixedHeight-scrollHeight-37+"px");	
			}
		}
		else{
			$(".js-fixed-tab").css("top","0").css("z-index","auto").removeClass("active");
		}		
	}	
}	

$(window).scroll(function(){
	if($(".js-fixed-wrap").length > 0) {
		tablescrolled();
	}
});

$(window).load(function(){
	if($(".js-fixed-wrap").length > 0) {
		tablescrolled();
	}
});
// scroll table

// scroll sidebar
function floatbar(){
	var winW=viewport().width;
	if(winW>1024){
		var headH = $(".header").height();
		var headFixed = $(".fixed-head").height();
		var floatPosGet = $(".js-float-cols").position();
		var floatPos = floatPosGet.top;
		var floatPosNew = headH+floatPos;
		var floatPosFixed = floatPosNew-headFixed;
		var floatBlock = $(".js-float-cols").height();
		var floatBar = $(".js-float-bar").height();
		var floatEnd = floatPosFixed+floatBlock-floatBar;
		if( $(window).scrollTop() > floatPosFixed){
			var scrollTop = $(window).scrollTop();	
			$(".js-float-bar").css("margin-top",scrollTop-floatPosFixed+"px");
			if( $(window).scrollTop() > floatEnd){

				$(".js-float-bar").css("margin-top",floatEnd-floatPosFixed+"px");
			}			
		}
		else{
			$(".js-float-bar").css("margin-top","0");
		}			
	}
	if(winW<=1024){
		var headH = $(".header").height();
		var floatPosGet = $(".js-float-cols").position();
		var floatPos = floatPosGet.top;
		var floatPosNew = headH+floatPos;
		var floatBlock = $(".js-float-cols").height();
		var floatBar = $(".js-float-bar").height();
		var floatEnd = floatPosNew+floatBlock-floatBar;
		if( $(window).scrollTop() > floatPosNew){
			var scrollTop = $(window).scrollTop();	
			$(".js-float-bar").css("margin-top",scrollTop-floatPosNew+"px");
			if( $(window).scrollTop() > floatEnd){

				$(".js-float-bar").css("margin-top",floatEnd-floatPosNew+"px");
			}			
		}
		else{
			$(".js-float-bar").css("margin-top","0");
		}
	}	
}	

$(window).scroll(function(){
	if($(".js-float-wrap").length > 0) {
		floatbar();
	}
});

$(window).load(function(){
	if($(".js-float-wrap").length > 0) {
		floatbar();
	}
});
// scroll sidebar


$('.payment-links__link').click(function() {	
  var target = $(this).attr('href');
	$('html, body').animate({scrollTop: $(target).offset().top - 60}, 1000);
	$('.js-toggle').click();
	return false;
});

$(".accordion__item").on("click", function(){
  $(".accordion__item").removeClass("open").removeClass("active");
$(this).find(".div-table-col:nth-child(1)").toggleClass("rotate");
$(this).find(".accordion__heading-mobile .div-table-col:nth-child(3)").toggleClass("rotate");
  $(this).addClass("active");
  $(this).find(".accordion__text").slideToggle().toggleClass("open"); 
  $(this).nextAll().find(".accordion__text").slideUp();
  $(this).nextAll().find(".div-table-col:nth-child(1)").removeClass("rotate");
  $(this).nextAll().find(".accordion__heading-mobile .div-table-col:nth-child(3)").removeClass("rotate");
  $(this).prevAll().find(".accordion__text").slideUp();
  $(this).prevAll().find(".div-table-col:nth-child(1)").removeClass("rotate");
  $(this).prevAll().find(".accordion__heading-mobile .div-table-col:nth-child(3)").removeClass("rotate");
});		

$(".payment-nav__link").on("click", function(){
	$(".payment-nav-cnt").slideDown("slow");
});

$('.payment-nav__link').click(function() {	
  var target = $(this).attr('href');
	$('html, body').animate({scrollTop: $(target).offset().top - 60}, 1000);
	$('.js-toggle').click();
	return false;
});

if ($('#map-delivery').length) {
	  ymaps.ready(function() {
	      var myMap = new ymaps.Map('map-delivery', {
	              center: [55.7151745690214,37.57112049999994],		                          
	              zoom: 16
	          }, { 
	          }),
	      BalloonContentLayout = ymaps.templateLayoutFactory.createClass('<div class="ballon"><p><span class="ballon__title">проверка формы добвавления</span><br>1т / 25м.куб.<br>Волгоград<i class="fa fa-long-arrow-right"></i>Анапа<br><span class="ballon-price">1 500<span class="rub">а</span></span>/час наличнымы<br><a href="">подробнее</a></p></div>');
	     
	      myPlacemark = new ymaps.Placemark(myMap.getCenter(), {
	          hintContent: ''
	      }, {
	          
	      });
	      myMap.geoObjects.add(myPlacemark);

	      if( /Android|webOS|iPhone|iPad|iPod|BlackBerry/i.test(navigator.userAgent) ) {
	          myMap.behaviors.disable('multiTouch');
	          myMap.behaviors.disable('scrollZoom');
	          myMap.behaviors.disable('drag');
	      }
	  });
	}

